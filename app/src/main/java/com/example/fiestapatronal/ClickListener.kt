package com.example.fiestapatronal

import android.view.View

interface ClickListener {
    fun onClick(vista: View, index:Int)
}