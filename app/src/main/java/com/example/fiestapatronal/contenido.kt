package com.example.fiestapatronal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_contenido.*
import kotlinx.android.synthetic.main.activity_contenido2.*

class contenido : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contenido)

        val bundle: Bundle? = intent.extras
        bundle?.let { bundleLibriDeNull ->
            val index = bundleLibriDeNull.getInt("key_index")
            val fiesta = ArrayList<Lista2>()

            fiesta.add(
                Lista2(
                    "Col. Centro",
                    "Se celebra: \nSan Francisco de Asis. \nEl dia de: 15 de agosto. \nPara alrededor de: \n500 Personas",
                    R.drawable.asis
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Benito Juarez",
                    "Se celebra: \nInmaculado Corazon de Maria. \nEl dia de: \n3 de Junio. \nPara alrededor de: \n400 Personas",
                    R.drawable.corazonmaria
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Marco Antonio Muñoz",
                    "Se celebra: \nNuestra Señora de los Dolores. \nEl dia de: \n15 de Septiembre. \nPara alrededor de: \n350 Personas",
                    R.drawable.dolores
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Linda Vista",
                    "Se celebra: \nSan Juan Bautista. \nEl dia de: \n24 de Junio. \nPara alrededor de: \n300 Personas",
                    R.drawable.bautista
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Pedregal 1 y Pedregal 2",
                    "Se celebra: \nNuestra Señora de Fatima. \nEl dia de: \n13 de Mayo. \nPara alrededor de: \n500 Personas",
                    R.drawable.fati2
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Puerto Palchan",
                    "Se celebra: \nNuestra Señor de Guadalupe. \nEl dia de: \n12 de Diciembre. \nPara alrededor de: \n300 Personas",
                    R.drawable.guadalupe
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Plan de la Vieja",
                    "Se celebra: \nLa Inmaculada Concepcion. \nEl dia de: \n8 de Diciembre. \nPara alrededor de: \n250 Personas",
                    R.drawable.concepcion
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Mateo Acosta",
                    "Se celebra: \nCristo Rey. \nEl dia de: \n23 de Noviembre \nPara alrededor de: \n200 Personas",
                    R.drawable.cristorey
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Lomas de las Flores",
                    "Se celebra: \nLa Sagrada Familia. \nEl dia de: \n30 de Deciembre. \nPara alrededor de: \n200 Personas",
                    R.drawable.sagradafamilia
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Teresita Peñafiel",
                    "Se celebra: \nSagrado corazon de Jesus. \nEl dia de: \n19 de Junio. \nPara alrededor de: \n400 Personas",
                    R.drawable.corazonjesus
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Francisco Villaraus",
                    "Se celebra: \nNuestro Señora de Guadalupe. \nEl dia de: \n12 de Diciembre. \nPara alrededor de: \n300 Personas",
                    R.drawable.guadalupe
                )
            )
            fiesta.add(
                Lista2(
                    "Col. 20 de Octubre",
                    "Se celebra: \nSan Judas Tadeo. \nEl dia de: \n28 de Octubre. \nPara alrededor de: \n350 Personas",
                    R.drawable.tadeo
                )
            )
            fiesta.add(
                Lista2(
                    "Col. Francisco I. Madero",
                    "Se celebra: \nMaria Auxiliadora. \nEl dia de: \n24 de Mayo. \nPara alrededor de: \n250 Personas",
                    R.drawable.mariaauxiliadora
                )
            )
            titu.setText(fiesta.get(index).lugar)
            descri.setText(fiesta.get(index).info)
            foto.setImageResource(fiesta.get(index).foto)
        }

    }
}