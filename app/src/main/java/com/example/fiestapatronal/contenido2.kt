package com.example.fiestapatronal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_contenido2.*

class contenido2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contenido2)

        val bundle: Bundle? = intent.extras
        bundle?.let { bundleLibriDeNull ->
            val index = bundleLibriDeNull.getInt("key_index")
            val fiesta = ArrayList<Lista2>()
            fiesta.add(
                Lista2(
                    "Misantla", "Se celebra:\n Nuestra Señora de la Asunción. \nEl dia de:\n15 de Agosto. \nPara alrededor de:\n 6,000 Personas", R.drawable.asuncion
                )
            )
            fiesta.add(
                Lista2(
                    "Km 3", "Se celebra: \nSagrado Corazón de Jesús. \nEl dia de:\n19 de Octubre. \nPara alrededor de:\n 300  Personas", R.drawable.corazonjesus
                )
            )
            fiesta.add(
                Lista2(
                    "Km  ½ ", "Se celebra: \nCapilla de la casa de la Iglesia. \nEl dia de:\n26 de Noviembre. \nPara alrededor de: \n300  Personas", R.drawable.casadelaiglecia
                )
            )
            fiesta.add(
                Lista2(
                    "Km 9 (La Reforma) ", "Se celebra: \nSan José. \nEl dia de: \n19 Marzo. \nPara alrededor de:\n 250  Personas", R.drawable.san_jose
                )
            )
            fiesta.add(
                Lista2(
                    "Venustiano Carranza (Galeras)", "Se celebra: \nNuestra Señora de Guadalupe. \nEl dia de: \n12 de  Diciembre. \nPara alrededor de:\n300  Personas ", R.drawable.guadalupe
                )
            )
            fiesta.add(
                Lista2(
                    "Vicente Guerrero", "Se celebra:\n Nuestra Señora de Guadalupe. \nEl dia de: \n12 de  Diciembre. \nPara alrededor de: \n350  Personas ", R.drawable.guadalupe
                )
            )
            fiesta.add(
                Lista2(
                    "Plan de Guerrero ", "Se celebra: \nCristo Rey. \nEl dia de:\n20 de Noviembre. \nPara alrededor de: \n350  Personas", R.drawable.cristorey
                )
            )
            fiesta.add(
                Lista2(
                    "El Pozón", "Se celebra:\n Nuestra Señora de Guadalupe. \nEl dia de:\n12 de  Diciembre. \nPara alrededor de:\n300  Personas", R.drawable.guadalupe
                )
            )

            fiesta.add(
                Lista2(
                    "La Mohonera", "Se celebra: \nSagrada Familia. \nEl dia de:\n30 de Diciembre. \nPara alrededor de:\n200  Personas", R.drawable.sagradafamilia
                )
            )

            fiesta.add(
                Lista2(
                    "Palma Sola de Ramírez", "Se celebra:\nSanta Cecilia. \nEl dia de:\n22 de Noviembre. \nPara alrededor de:\n350  Personas ", R.drawable.santacecilia
                )
            )

            fiesta.add(
                Lista2(
                    "Santa Cruz Buena Vista", "Se celebra:\n Santa Cruz. \nEl dia de:\n3 de Mayo. \nPara alrededor de:\n400  Personas", R.drawable.santacruz
                )
            )

            fiesta.add(
                Lista2(
                    "Lomas de Mirasol", "Se celebra:\n La Candelaria. \nEl dia de:\n2 de Febrero. \nPara alrededor de:\n250  Personas ", R.drawable.candelaria
                )
            )
            fiesta.add(
                Lista2(
                    "Mirasol", "Se celebra: \nSan José. \nEl dia de:\n19 de Marzo. \nPara alrededor de:\n350  Personas ", R.drawable.san_jose
                )
            )
            fiesta.add(
                Lista2(
                    "El lirial", "Se celebra:\n Sagrado Corazón de Jesús. \nEl dia de:\n23 de Junio. \nPara alrededor de:\n150  Personas", R.drawable.corazonjesus
                )
            )
            fiesta.add(
                Lista2(
                    "Las Lomas Francisco I. Madero", "Se celebra:\n Sagrado Corazón de Jesús. \nEl dia de:\n23 de Junio. \nPara alrededor de:\n300 personas", R.drawable.corazonjesus
                )
            )
            fiesta.add(
                Lista2(
                    "Guayabal", "Se celebra:\n Virgen del Carmen. \nEl dia de:\n16 de Julio. \nPara alrededor de:\n250 personas", R.drawable.carmen
                )
            )

            fiesta.add(
                Lista2(
                    "Independencia", "Se celebra: \nSan José. \nEl dia de:\n19 de Marzo. \nPara alrededor de:\n250 Personas", R.drawable.san_jose
                )
            )
            fiesta.add(
                Lista2(
                    "La Palma Buenos Aires", "Se celebra: \nSagrado Corazón de Jesús. \nEl dia de:\n23 de Junio. \nPara alrededor de:\n350 personas", R.drawable.corazonjesus
                )
            )
            fiesta.add(
                Lista2(
                    "Santa Julia", "Se celebra:\n Santa Julia. \nEl dia de:\n10 de Diciembre. \nPara alrededor de:\n400 Personas", R.drawable.santajulia
                )
            )


            fiesta.add(
                Lista2(
                    "Morelos Plan", "Se celebra:\n Nuestra Señora de Fátima. \nEl dia de:\n13 de Mayo. \nPara alrededor de:\n450 Personas", R.drawable.fati2
                )
            )
            fiesta.add(
                Lista2(
                    "Chapa Chapa", "Se celebra:\n San Francisco de Asís. \nEl dia de:\n4 de Octubre. \nPara alrededor de:\n400 Personas", R.drawable.asis
                )
            )
            fiesta.add(
                Lista2(
                    "Plan de la Vieja", "Se celebra:\n Nuestra Señora del Carmen. \nEl dia de:\n16 de Julio. \nPara alrededor de:\n350 Personas", R.drawable.carmen
                )
            )
            fiesta.add(
                Lista2(
                    "Arroyo Frio", "Se celebra: \nSan Francisco de Asís. \nEl dia de:\n4 de Octubre. \nPara alrededor de:\n500 Personas", R.drawable.asis
                )
            )
            fiesta.add(
                Lista2(
                    "Morelos Capilla", "Se celebra: \nSanta Teresita del Niño Jesús. \nEl dia de:\n3 de Octubre. \nPara alrededor de:\n450 Personas", R.drawable.teresita
                )
            )
            fiesta.add(
                Lista2(
                    "Tapapulum (Plan)", "Se celebra: \nSan Antonio Abad. \nEl dia de:\n17 de Enero. \nPara alrededor de:\n300 Personas", R.drawable.antonio
                )
            )
            fiesta.add(
                Lista2(
                    "Tapapulum (Arriba)", "Se celebra: \nSan Antonio Abad. \nEl dia de:\n24 de Junio. \nPara alrededor de:\n450 Personas", R.drawable.antonio
                )
            )
            fiesta.add(
                Lista2(
                    "Las Peñitas", "Se celebra:\n San Martín de Porres. \nEl dia de:\n3 de Noviembre. \nPara alrededor de:\n250 Personas", R.drawable.martin
                )
            )
            fiesta.add(
                Lista2(
                    "La Habana", "Se celebra: \nSan Antonio de Padua. \nEl dia de:\n3 de Mayo. \nPara alrededor de:\n300 Personas", R.drawable.padua
                )
            )
            fiesta.add(
                Lista2(
                    "Ignacio Allende", "Se celebra:\n Nuestra Señora de Guadalupe. \nEl dia de:\n12 de  Diciembre. \nPara alrededor de:\n350  Personas", R.drawable.guadalupe
                )
            )
            fiesta.add(
                Lista2(
                    "Ejido Morelos", "Se celebra:\n San Judas Tadeo. \nEl dia de:\n28 de Octubre. \nPara alrededor de:\n400 Personas", R.drawable.tadeo
                )
            )
            fiesta.add(
                Lista2(
                    "Moxillón", "Se celebra: \nNuestra Señora de la Candelaria. \nEl dia de:\n2 de Febrero. \nPara alrededor de:\n350 Personas", R.drawable.candelaria
                )
            )
            fiesta.add(
                Lista2(
                    "Rancho Viejo", "Se celebra:\n La Sagrada Familia. \nEl dia de:\nÚltimo domingo de Diciembre. \nPara alrededor de:\n350 Personas", R.drawable.sagradafamilia
                )
            )
            fiesta.add(
                Lista2(
                    "La Mesa", "Se celebra:\n San Juan Bautista. \nEl dia de:\n24 de Junio. \nPara alrededor de:\n350 Personas", R.drawable.bautista
                )
            )
            fiesta.add(
                Lista2(
                    "Santa Cruz Hidalgo", "Se celebra: \nLa Santa Cruz. \nEl dia de:\n3 de Mayo. \nPara alrededor de:\n450 Personas", R.drawable.santacruz
                )
            )
            fiesta.add(
                Lista2(
                    "Loma del Cojolite", "Se celebra:\n San Isídro Labrador. \nEl dia de:\n15 de Mayo. \nPara alrededor de:\n350 Personas", R.drawable.labrador
                )
            )
            fiesta.add(
                Lista2(
                    "Espaldilla", "Se celebra:\n Nuestra Señora del Carmen. \nEl dia de:\n16 de Julio. \nPara alrededor de:\n300 Personas", R.drawable.carmen
                )
            )
            fiesta.add(
                Lista2(
                    "El Carmen", "Se celebra:\n La Virgen del Carmen. \nEl dia de:\n16 de Julio. \nPara alrededor de:\n350 Personas", R.drawable.carmen
                )
            )
            fiesta.add(
                Lista2(
                    "La Lima", "Se celebra:\n Nuestra Señora de Guadalupe. \nEl dia de:\n12 de  Diciembre. \nPara alrededor de:\n300  Personas ", R.drawable.guadalupe
                )
            )
            fiesta.add(
                Lista2(
                    "El Nogal", "Se celebra:\n San Juan Bautista. \nEl dia de:\n24 de Junio. \nPara alrededor de:\n350 Personas", R.drawable.bautista
                )
            )
            fiesta.add(
                Lista2(
                    "El Porvenir", "Se celebra: \nVirgen de los Remedios. \nEl dia de:\n1 de Septiembre. \nPara alrededor de:\n250 Personas", R.drawable.remedios
                )
            )
            fiesta.add(
                Lista2(
                    "Arroyo Negro", "Se celebra: \nVirgen de Fátima. \nEl dia de:\n13 de Mayo. \nPara alrededor de:\n200 Personas", R.drawable.fati2
                )
            )
            fiesta.add(
                Lista2(
                    "Salvador Díaz Mirón", "Se celebra:\n San Isídro Labrador. \nEl dia de:\n15 de Mayo. \nPara alrededor de:\n500 personas", R.drawable.labrador
                )
            )
            fiesta.add(
                Lista2(
                    "Ejido Guadalupe Victoria", "Se celebra:\n Divino Salvador. \nEl dia de:\n6 de Agosto. \nPara alrededor de:\n350 Personas", R.drawable.divinosalvador
                )
            )
            fiesta.add(
                Lista2(
                    "Plan Grande", "Se celebra: \nRafael Guízar y Valencia. \nEl dia de:\n23 de Octubre. \nPara alrededor de:\n350 Personas", R.drawable.rafael
                )
            )
            fiesta.add(
                Lista2(
                    "Paso Blanco", "Se celebra:\n Inmaculada Concepción. \nEl dia de:\n8 de Diciembre. \nPara alrededor de:\n450 Personas", R.drawable.concepcion
                )
            )
            fiesta.add(
                Lista2(
                    "Manuel Gutiérrez Nájera (San Lorenzo)", "Se celebra: \nSan Lorenzo. \nEl dia de:\n10 de Agosto. \nPara alrededor de:\n350 Personas", R.drawable.lorenzo
                )
            )
            fiesta.add(
                Lista2(
                    "Pueblo Viejo", "Se celebra:\n San Miguel Arcángel. \nEl dia de:\n29 de Septiembre. \nPara alrededor de:\n400 Personas", R.drawable.migue
                )
            )
            titulito1.setText(fiesta.get(index).lugar)
            descricionito.setText(fiesta.get(index).info)
            fotito.setImageResource(fiesta.get(index).foto)
        }
    }
}