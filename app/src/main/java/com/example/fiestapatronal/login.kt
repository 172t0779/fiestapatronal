package com.example.fiestapatronal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.DatabaseReference
import kotlinx.android.synthetic.main.activity_login.*
import kotlin.properties.Delegates

class login : AppCompatActivity() {
    private var email by Delegates.notNull<String>()
    private var contr by Delegates.notNull<String>()
    private lateinit var edtEmail: EditText
    private lateinit var edtPassword: EditText
    lateinit var mGoogleSignInClient: GoogleSignInClient
    val RC_SIGN_IN: Int = 123
    lateinit var gso: GoogleSignInOptions
    private lateinit var mAuth: FirebaseAuth
    override fun onStart() {
        super.onStart()
        val user = mAuth.currentUser
        if (user != null) {
            val intent = Intent(this, login::class.java)
            startActivity(intent)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btn.setOnClickListener {
            edtEmail = findViewById(R.id.edtUser)
            edtPassword = findViewById(R.id.edtContrasena)
            mAuth = FirebaseAuth.getInstance()
            email = edtEmail.text.toString()
            contr = edtPassword.text.toString()

            if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(contr)) {
                if (email == "Admin" && contr == "1234") {
                    startActivity(Intent(this, Menu::class.java))

                    Toast.makeText(this, "Bienvenido!!!", Toast.LENGTH_SHORT).show()
                } else {
                    mAuth.signInWithEmailAndPassword(email, contr)
                        .addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {

                                startActivity(Intent(this, Menu::class.java))


                            } else {
                                Toast.makeText(
                                    this, "Error",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                }
            } else {
                Toast.makeText(this, "Datos de usuario no encotnrados", Toast.LENGTH_SHORT).show()
            }
        }
        tvRegistrate.setOnClickListener {
            startActivity(Intent(this, registro::class.java))
        }
        mAuth = FirebaseAuth.getInstance()

        create()
        google.setOnClickListener {
            signIn();
        }

    }
private fun create(){
        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
}
    private fun signIn() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            val exception = task.exception

            try {
                val account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                Toast.makeText(this, "Iniciar Sesión Fallida", Toast.LENGTH_SHORT)
                    .show()
            }

        }
    }
    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)

        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val bundle = Bundle()
                    bundle.apply {
                        putString("USUARIO", "Google")
                    }
                    val intent = Intent(this, Menu::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)

                    Toast.makeText(this, "Bienvenido!!!", Toast.LENGTH_SHORT).show()

                } else {
                    Toast.makeText(this, "Login Fallida ", Toast.LENGTH_SHORT).show()
                }
            }
    }


}