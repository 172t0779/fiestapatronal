package com.example.fiestapatronal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class opcion1 : AppCompatActivity() {
    var lista: RecyclerView? = null
    var adaptador:AdaptadorCustom? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opcion1)

        val fiesta = ArrayList<Lista>()
        fiesta.add(Lista("Misantla" ,R.drawable.asuncion))
        fiesta.add(Lista("Km 3" , R.drawable.corazonjesus))
        fiesta.add(Lista("Km  ½ " , R.drawable.casadelaiglecia))
        fiesta.add(Lista("Km 9 (La Reforma) " ,R.drawable.san_jose))
        fiesta.add(Lista("Venustiano \nCarranza (Galeras)",R.drawable.guadalupe))
        fiesta.add(Lista("Vicente Guerrero" , R.drawable.guadalupe))
        fiesta.add(Lista("Plan de Guerrero " ,R.drawable.cristorey))
        fiesta.add(Lista("El Pozón" ,R.drawable.guadalupe))
        fiesta.add(Lista("La Mohonera" ,R.drawable.sagradafamilia))
        fiesta.add(Lista("Palma Sola de Ramírez" , R.drawable.santacecilia))
        fiesta.add(Lista("Santa Cruz\n Buena Vista" ,R.drawable.santacruz))
        fiesta.add(Lista("Lomas de Mirasol" , R.drawable.candelaria))
        fiesta.add(Lista("Mirasol" , R.drawable.san_jose))
        fiesta.add(Lista("El lirial" , R.drawable.corazonjesus))
        fiesta.add(Lista("Las Lomas \nFrancisco I. Madero" , R.drawable.corazonjesus))
        fiesta.add(Lista("Guayabal" , R.drawable.carmen))
        fiesta.add(Lista("Independencia" , R.drawable.san_jose))
        fiesta.add(Lista("La Palma \nBuenos Aires" , R.drawable.corazonjesus))
        fiesta.add(Lista("Santa Julia" , R.drawable.santajulia))
        fiesta.add(Lista("Morelos Plan" , R.drawable.fati2))
        fiesta.add(Lista("Chapa Chapa" , R.drawable.asis))
        fiesta.add(Lista("Plan de la Vieja" , R.drawable.carmen))
        fiesta.add(Lista("Arroyo Frio" ,R.drawable.asis))
        fiesta.add(Lista("Morelos Capilla" , R.drawable.teresita))
        fiesta.add(Lista("Tapapulum (Plan)" , R.drawable.antonio))
        fiesta.add(Lista("Tapapulum (Arriba)" , R.drawable.bautista))
        fiesta.add(Lista("Las Peñitas" , R.drawable.martin))
        fiesta.add(Lista("La Habana" , R.drawable.padua))
        fiesta.add(Lista("Ignacio Allende" , R.drawable.guadalupe))
        fiesta.add(Lista("Ejido Morelos" , R.drawable.tadeo))
        fiesta.add(Lista("Moxillón" , R.drawable.candelaria))
        fiesta.add(Lista("Rancho Viejo" ,R.drawable.sagradafamilia))
        fiesta.add(Lista("La Mesa" , R.drawable.bautista))
        fiesta.add(Lista("Santa Cruz Hidalgo" , R.drawable.santacruz))
        fiesta.add(Lista("Loma del Cojolite" , R.drawable.labrador))
        fiesta.add(Lista("Espaldilla", R.drawable.carmen))
        fiesta.add(Lista("El Carmen" , R.drawable.carmen))
        fiesta.add(Lista("La Lima" , R.drawable.guadalupe))
        fiesta.add(Lista("El Nogal" , R.drawable.bautista))
        fiesta.add(Lista("El Porvenir" , R.drawable.remedios))
        fiesta.add(Lista("Arroyo Negro" ,R.drawable.fati2))
        fiesta.add(Lista("Salvador Díaz Mirón" , R.drawable.labrador))
        fiesta.add(Lista("Ejido Guadalupe \n Victoria" , R.drawable.divinosalvador))
        fiesta.add(Lista("Plan Grande" , R.drawable.rafael))
        fiesta.add(Lista("Paso Blanco" , R.drawable.concepcion))
        fiesta.add(Lista("Manuel Gutiérrez \n Nájera (San Lorenzo)" , R.drawable.lorenzo))
        fiesta.add(Lista("Pueblo Viejo" , R.drawable.migue))

        lista = findViewById(R.id.Listamin)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorCustom(this,fiesta, object: ClickListener{
            override fun onClick(vista: View, index: Int) {
                val bundle = Bundle()
                bundle.apply {
                    putInt("key_index", index)
                }
                val intent = Intent(applicationContext, contenido2::class.java).apply {
                    putExtras(bundle)
                }
                startActivity(intent)

            }
        })

        lista?.adapter = adaptador
    }
}