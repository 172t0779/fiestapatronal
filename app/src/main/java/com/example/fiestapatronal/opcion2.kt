package com.example.fiestapatronal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class opcion2 : AppCompatActivity() {
    var lista: RecyclerView? = null
    var adaptador: AdaptadorCustom? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opcion2)

        val fiesta = ArrayList<Lista>()

        fiesta.add(
            Lista(
                "Col. Centro",
                R.drawable.asis
            )
        )
        fiesta.add(
            Lista(
                "Col. Benito Juarez",
                R.drawable.corazonmaria
            )
        )
        fiesta.add(
            Lista(
                "Col. Marco Antonio Muñoz",
                R.drawable.dolores
            )
        )
        fiesta.add(
            Lista(
                "Col. Linda Vista",
                R.drawable.bautista
            )
        )
        fiesta.add(
            Lista(
                "Col. Pedregal 1 \ny Pedregal 2",
                R.drawable.fati2
            )
        )
        fiesta.add(
            Lista(
                "Col. Puerto Palchan",
                R.drawable.guadalupe
            )
        )
        fiesta.add(
            Lista(
                "Col. Plan de \nla Vieja",
                R.drawable.concepcion
            )
        )
        fiesta.add(
            Lista(
                "Col. Mateo Acosta",
                R.drawable.cristorey
            )
        )
        fiesta.add(
            Lista(
                "Col. Lomas de las\n Flores",
                R.drawable.sagradafamilia
            )
        )
        fiesta.add(
            Lista(
                "Col. Teresita Peñafiel",
                R.drawable.corazonjesus
            )
        )
        fiesta.add(
            Lista(
                "Col. Francisco \nVillaraus",
                R.drawable.guadalupe
            )
        )
        fiesta.add(
            Lista(
                "Col. 20 de Octubre", R.drawable.tadeo
            )
        )
        fiesta.add(
            Lista(
                "Col. Francisco\n I. Madero", R.drawable.mariaauxiliadora
            )
        )

        lista = findViewById(R.id.Lista)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorCustom(this, fiesta, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                val bundle = Bundle()
                bundle.apply {
                    putInt("key_index", index)
                }
                val intent = Intent(applicationContext, contenido::class.java).apply {
                    putExtras(bundle)
                }
                startActivity(intent)

            }
        })

        lista?.adapter = adaptador


    }
}