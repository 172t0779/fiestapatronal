package com.example.fiestapatronal

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_registro.*
import kotlin.properties.Delegates


class registro : AppCompatActivity() {
    private lateinit var uno: EditText
    private lateinit var dos: EditText
    private lateinit var tres: EditText
    private lateinit var cuatro: EditText

    private var nomb by Delegates.notNull<String>()
    private var apellido by Delegates.notNull<String>()
    private var email by Delegates.notNull<String>()
    private var contra by Delegates.notNull<String>()
    private var termino by Delegates.notNull<String>()
    private lateinit var databaseReference: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)
        iniciar()
    }

    private fun iniciar() {
        uno= findViewById(R.id.nom)
        dos = findViewById(R.id.apellidos)
        tres = findViewById(R.id.us)
        cuatro = findViewById(R.id.con)
        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()
        databaseReference = database.reference.child("Usuarios")

    }
    fun btn_guardar(view: View) {
        crearUsuario()
    }

    private fun crearUsuario(){
        nomb = uno.text.toString()
        apellido = dos.text.toString()
        email = tres.text.toString()
        contra = cuatro.text.toString()

        termino = if (chkTerminos.isChecked) "si" else ""

        if (!TextUtils.isEmpty(nomb) && !TextUtils.isEmpty(apellido) && !TextUtils.isEmpty(email)
            && !TextUtils.isEmpty(contra)) {
            auth.createUserWithEmailAndPassword(email, contra)
                .addOnCompleteListener(this) {
                    val user: FirebaseUser = auth.currentUser!!
                    verifyEmail(user)
                    val currentUserDb = databaseReference.child(user.uid)
                    currentUserDb.child("Nombre").setValue(nomb)
                    currentUserDb.child("Apellidos").setValue(apellido)
                    currentUserDb.child("Correo").setValue(email)
                    currentUserDb.child("Contraseña").setValue(contra)

                    startActivity(Intent(this, login::class.java))

                }.addOnFailureListener {
                    Toast.makeText(
                        this, "Error en la autenticación.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
        } else {
            Toast.makeText(this, "Verifique de nuevo", Toast.LENGTH_SHORT).show()
        }
    }

private fun verifyEmail(user: FirebaseUser) {
    user.sendEmailVerification()
        .addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                Toast.makeText(
                    this,
                    "Email " + user.getEmail(),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    this,
                    "Error al verificar el correo ",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
}

}